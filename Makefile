OBJ= fileEntry.o strmap.o tracker.o
DEF= fileEntry.h strmap.h
CC= g++
WARNING = -Wall -Werror -g
LIB=

todo: tracker

fileEntry.o: fileEntry.cpp fileEntry.h 
	$(CC) -c fileEntry.cpp $(WARNING)

strmap.o: strmap.cpp strmap.h 
	$(CC) -c strmap.cpp $(WARNING)

tracker.o: tracker.cpp $(DEF)
	$(CC) -c tracker.cpp -lpthread $(WARNING)

tracker: $(OBJ)
	$(CC) $(OBJ) -o tracker $(LIB)
	

clean: 
	rm $(OBJ)
	rm tracker
