// ./tracker -tPort 5556

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <csignal>
#include <algorithm>

#include <unistd.h>
#include <string>

#include "strmap.h"
#include "fileEntry.h"

#define DEFAULT_PORT    5556
#define DEFAULT_IP      "127.0.0.1"

#define MAX_MSG_SIZE    1024
#define MAX_QUEUE       10

char* commandProcessing(char* msg, StrMap* map_archivos, StrMap* map_clientes, int socket);
void print_pollfd(struct pollfd *pfd);
void parseCommand(char* cmd,char* prm,char* buffer);

int listen_socket;
int new_socket;

char* getCmdOption(char ** begin, char ** end, const std::string & option) {
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end) {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option) {
    return std::find(begin, end, option) != end;
}

void exitCTRL(int sig) {
    close(listen_socket);
    close(new_socket);
    exit(1);
}

int main(int argc, char *argv[]) 
{
    listen_socket   = -1;
    new_socket      =  1;
    int error           =  1;
    int on              =  1;
    struct sockaddr_in  address;
    struct pollfd fds[200];
    int    nfds         = 1;
    int current_size    = 0;
    int timeout         = 0;
    int i,j;
    int shutdown_tracker        = 0;
    int close_socket            = 0;
    int clean_structure         = 0;
    int namelen;
    char* buffer;
    int cant_bytes;
    char* client_answer;
    int socketID;
    
    StrMap* map_archivos = newHash(1000);
    StrMap* map_clientes = newHash(50);
    
    signal(SIGINT, exitCTRL);
    
    const char* host = DEFAULT_IP;
    printf("Tracker IP: %s\n",host);

    int tracker_port;
    if(cmdOptionExists(argv, argv+argc, "-tPort")) {
        tracker_port = atoi(getCmdOption(argv, argv + argc, "-tPort"));
    }
    else {
        tracker_port = DEFAULT_PORT;
    }
    printf("Tracker Port: %i\n",tracker_port);
    
    // Creo el socket de bienvenida
    listen_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_socket < 0) { perror("[ERROR] socket()"); exit(-1); } // bye!!
  	
    // Le digo al socket que puede ser reutilizable
    error = setsockopt(listen_socket, SOL_SOCKET,  SO_REUSEADDR,
            (char *)&on, sizeof(on));
    if (error < 0)  { perror("[ERROR] setsockopt()"); close(listen_socket); exit(-1); } // chau!!
    
    // y ahora le digo que sea no bloqueante
    error = ioctl(listen_socket, FIONBIO, (char *)&on);
    if (error < 0) { perror("[ERROR] ioctl()"); close(listen_socket); exit(-1); }// adieu !!
    
    // Hago el binding
    memset(&address, 0, sizeof(address));
    address.sin_family      = AF_INET;
    address.sin_addr.s_addr = inet_addr(host);
    address.sin_port        = htons(tracker_port);
    error = bind(listen_socket, (struct sockaddr *)&address, sizeof(address));
    while (error < 0) {
        printf("[ERROR] Puerto ocupado\n");
        address.sin_port        = 0;
        error = bind(listen_socket, (struct sockaddr *)&address, sizeof(address));
        namelen = sizeof(address);
        if (getsockname(listen_socket, (struct sockaddr *) &address,(socklen_t*) &namelen) < 0) {
            printf("getsockname()\n");
            exit(-1);
        }
        printf("Listen Port asignado: %d\n\n", ntohs(address.sin_port));
    } 
    
    // Me pongo a escuchar [:o
    error = listen(listen_socket, MAX_QUEUE);
    if (error < 0) { perror("[ERROR] listen()"); close(listen_socket); exit(-1); }
  
    // Inicializo la estructura de poll y agrego el socket de bienvenida en el
    // primer lugar
    memset(fds, 0 , sizeof(fds));

    fds[0].fd = listen_socket;
    fds[0].events = POLLIN | POLLRDHUP; // Eventos que escucha

    timeout = (-1); // Hasta el final de los tiempos
    
    // Aca empieza lo divertido 
    do {
        error = poll(fds, nfds, timeout);
        if (error < 0) { perror("[ERROR] poll()"); break; } // hago un break para poder cerrar todo antes de irme
 	if (error == 0) { printf("[ERROR] se acabo el tiempo de poll() \n"); break; } // idem
        
        // Analizamos a poll alias Paul
        current_size = nfds;
        for (i = 0; i < current_size; i++) {
            
            // Busco por todo Paul hasta que algun socket tenga un evento
            if(fds[i].revents == 0) {
                continue;
            }
            
            // Soy el que escucha
            if (fds[i].fd == listen_socket) {
                
                // Procedo a aceptar nuevas conexiones que estaban a la espera
                do {
                    
                    // Salgo del loop si da el error EWOULDBLOCK que indicaria 
                    // que no tenemos mas 
                    new_socket = accept(listen_socket, NULL, NULL);
                    if (new_socket < 0) {
                        if (errno != EWOULDBLOCK) {
                                perror(" [ERROR] accept() de nuevas conexiones");
                                shutdown_tracker = 1;
                        }
                        break; // LLEGA POR SER NO BLOQUEANTE
                    }
                    
                    // Agrego la conexion a la estructura del poll
                    fds[nfds].fd = new_socket;
                    fds[nfds].events = POLLIN | POLLRDHUP; // Eventos que escucha
                    nfds++;
                } while (new_socket != -1); // si es distino de -1 quiere decir que acepto una conexion y le asigna numero correlativos
                
            }
            else { // No soy el que escucha, con lo cual procedemos a analizar el poll
                
                do {
                    
                    // Primero que nada me fijo si esta 
                    if (fds[i].revents & POLLRDHUP) {
                        printf("-------------------------------------------\n\n");
                        printf("Se nos fue un cliente :( !!!\n\n");
                        
                        // Borramos un cliente
                        Entries *lista, *next;
                        fileEntry *target;
                        char* socket_char = new char[3];
                        sprintf(socket_char,"%d",fds[i].fd);
                        search(map_clientes,socket_char,lista);
                        target = lista->current;
                        char* ip = new char[16];
                        bzero(ip,16);
                        int port_num;
                        while (target != NULL) {
                                next = lista->next;
                                strcat(ip, target->getIP());
                                port_num = target->getPort();
                                delete lista;
                                if (next == NULL) {
                                        target = NULL;
                                }
                                else {
                                        target = next->current;
                                }
                                lista = next;
                        }
                        if (target == NULL) {
                                deleteClient(map_archivos,ip,port_num);
                                deleteClient(map_clientes,ip,port_num);
                        }
                        delete [] ip;
                        printf("Clientes activos:\n");
                        printf("\n************IMPRESION************\n");
                        printf("--- [Socket]-[IP]-[Puerto] ---\n\n");
                        printAll(map_clientes);
                        printf("\n*********FIN DE IMPRESION********\n");
                        printf("\n");
                        printf("Archivos compartidos:\n");
                        printf("\n************IMPRESION************\n");
                        printf("--- [Filename]-[IP]-[Puerto] ---\n\n");
                        printAll(map_archivos);
                        printf("\n*********FIN DE IMPRESION********\n");
                        printf("\n\n");
                        close_socket = 1; 
                        break;
                    }
                    
                    // Si esta me fijo si me esta queriendo enviar algo 
                    if (fds[i].revents & POLLIN) {
                        buffer = new char[MAX_MSG_SIZE];
                        bzero(buffer,MAX_MSG_SIZE);
                        cant_bytes = recv(fds[i].fd, buffer, MAX_MSG_SIZE, 0);
                        
                        if (cant_bytes < 0) { // @FIXME ver si no tendria que ser <=
                            if (errno != EWOULDBLOCK) {
                                perror("[ERROR] recv() ");
                                close_socket = 1;
                            }
                            break; // Me voy porque hubo un error o porque no habia nada 
                        }
                        
                        socketID = fds[i].fd;
                        client_answer = commandProcessing(buffer, map_archivos, map_clientes, socketID);
                        error = send(fds[i].fd, client_answer, strlen(client_answer), 0);
                        delete [] buffer; // **************************************************************
                        delete [] client_answer;
                        if (error < 0) { perror("  send() failed"); close_socket = 1; break; }
                        
                        ////////////////////////////////////////////////////////
                        // ACA VA TODO LO RELATIVO AL PROCESAMIENTO DE COMANDO//
                        ////////////////////////////////////////////////////////
                    }
                    break; // Me voy porque ya procese a este compañero 
                } while(1); // Alguien te va a quebrar antes no vas a estar corriedo 4ever
                
                if (close_socket) {
                    close(fds[i].fd);
                    fds[i].fd = -1;
                    clean_structure = 1; // Aviso que hay uno que no esta mas para poder borrarlo
                    close_socket = 0;
                }
            }
        }
        
        // Reordenamos el poll para no tomar aquellos que nos abandonaron
        if (clean_structure) {
            clean_structure = 0;
            for (i = 0; i < nfds; i++) {
                if (fds[i].fd == -1) {
                    for(j = i; j < nfds; j++) {
                        fds[j].fd = fds[j+1].fd;
                    }
                    nfds--;
                }
            }
        }
		
    } while (!shutdown_tracker); 
    
    // El ultimo que apague la luz (si queda algun socket abierto lo cierro)
    for (i = 0; i < nfds; i++) {
        if(fds[i].fd >= 0) {
                close(fds[i].fd);
        }
    }
	
}

/**
 * Procesa el mensaje siguiendo el Protocolo de comunicacion entre el cliente
 * y el tracker
 * 
 * @param msg
 * @return 
 */
char* commandProcessing(char* msg, StrMap* map_archivos, StrMap* map_clientes, int socket) {
    char* res = new char[MAX_MSG_SIZE];
    bzero(res,MAX_MSG_SIZE);
    char* token = NULL;
    char* ip = new char[16];
    bzero(ip,16);
    char* port = new char[6];
    bzero(port,6);
    char* filename = new char[MAX_MSG_SIZE];
    bzero(filename,MAX_MSG_SIZE);
    char* md5 = new char[33];
    bzero(md5,33);
    int port_num;
    Entries* lista;
    Entries* next;
    fileEntry* target;
    bool ok;
    char* socket_char = new char[3];
    sprintf(socket_char,"%d",socket);
    char * cmd = new char[10]; 
    char * prm = new char[256]; 
    parseCommand(cmd,prm,msg);
    
    printf("-------------------------------------------\n\n");
    
    token = strtok(msg,"\n");
    if (strncmp("NEWCLIENT", cmd, strlen("NEWCLIENT")) == 0) {
        token = strtok(prm,":");
        strcat(ip, token);
        token = strtok(NULL,"\n");
        strcat(port, token);
        printf("NUEVO CLIENTE\n");
        printf("IP: %s\n",ip);
        printf("Port: %s\n",port);
        
        int port_num = atoi(port);
        addFileOnTracker(map_clientes, socket_char, "", ip, port_num);
        
        printf("\n************IMPRESION************\n");
        printf("--- [Socket]-[IP]-[Puerto] ---\n\n");
        printAll(map_clientes);
        printf("\n*********FIN DE IMPRESION********\n");

        strcat(res,"ok");
    }
    else if (strncmp("PUBLISH", cmd, strlen("PUBLISH")) == 0) {
        token = strtok(NULL,"\n");
        strcat(filename, token);
        token = strtok(NULL,"\n");
        strcat(md5, token);
        printf("NUEVO ARCHIVO COMPARTIDO\n");
        printf("Filename: %s\n",filename);
        printf("MD5: %s\n",md5);
        
        search(map_clientes,socket_char,lista);
        target = lista->current;
        while (target != NULL) {
            next = lista->next;
            strcat(ip, target->getIP());
            port_num = target->getPort();
            delete lista;
            if (next == NULL) {
                target = NULL;
            }
            else {
                target = next->current;
            }
            lista = next;
        }
        
        addFileOnTracker(map_archivos, filename, md5, ip, port_num);
        
        printf("\n************IMPRESION************\n");
        printf("--- [Filename]-[IP]-[Puerto] ---\n\n");
        printAll(map_archivos);
        printf("\n*********FIN DE IMPRESION********\n");
        
        strcat(res,"ok");
    }
    else if (strncmp("SEARCH", cmd, strlen("SEARCH")) == 0) {
        token = strtok(NULL,"\n");
        strcat(filename, token);
        printf("NUEVA BUSQUEDA\n");
        printf("Filename: %s\n",filename);
        
        if (exists(map_archivos,filename)) {
            ok = search(map_archivos, filename, lista);
            if (ok == 0)
                printf("[ERROR] Nos dio un error en la busqueda !!!\n");
            target = lista->current;
            if (target != NULL) {
                md5 = target->getMD5();
            }
            if (md5 == NULL) {                                                      // NO EXISTE EL ARCHIVO
                printf("[ERROR] No existe el archivo\n");
                strcat(res,"ok");
            }
            else {                                                                  // EXISTE EL ARCHIVO
                strcat(res, "FILE\n");
                strcat(res,md5);
                char* aux = new char[22];
                bzero(aux,22);
                while (target != NULL) {
                        char* ip2 = new char[16];
                        bzero(ip2,16);
                    next = lista->next;
                    strcat(ip2, target->getIP());
                    port_num = target->getPort();
                    sprintf(aux,"\n%s:%i",ip2,port_num);
                    strcat(res,aux);
                    delete lista;
                    if (next == NULL) {
                        target = NULL;
                    }
                    else {
                        target = next->current;
                    }
                    lista = next;
                }
                printf("\nLe enviamos al cliente esto: \n%s\n",res);
            }
        }
        else {
            printf("[ERROR] No existe el archivo buscado\n");
            strcat(res,"fail");
        }
    }
    else {
        printf("\nCOMANDO INVALIDO\n");
        strcat(res,"Comando Invalido");
    }
    delete [] port;
    delete [] filename;
    delete [] cmd;
    delete [] prm;
    printf("\n\n");

    return res;
}

void parseCommand(char* cmd,char* prm,char* buffer) {
    int tam_prm = 256;
    int i   = 0;
    char c  = buffer[i];
    cmd[i]= buffer[i];
    while (c!=' ' && c!='\n' && i<9){
            i++;
            cmd[i]= buffer[i];
            c=buffer[i];	
    }

    cmd[i]='\0';
    i++;
    c=buffer[i];
    int j=0;
    while (c!='\0' && c!='\n' && c!=' ' && j<tam_prm){
            prm[j]= buffer[i];
            i++;
            j++;
            c=buffer[i];	
    }
    prm[j]='\0';
}
