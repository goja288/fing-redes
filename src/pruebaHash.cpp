#include <string>
#include "strmap.h"
#include "fileEntry.h"
#include <stdio.h>

using namespace std;

int main(void)
{
    StrMap *sm;
    Entries *first, *next;
    fileEntry *target;
    
    sm = newHash(2000);
    addFileOnTracker(sm, "name1", "md51", "192.168.0.1", 5551);
    addFileOnTracker(sm, "name2", "md52", "192.168.0.2", 5552);
    addFileOnTracker(sm, "name3", "md53", "192.168.0.3", 5553);
    addFileOnTracker(sm, "name4", "md54", "192.168.0.4", 5554);
    
    
    int  result = search(sm, "name3", first);
    if(result == 0) {
        printf("no se encontaron resultados.");
    }
    printf("\n");
    
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }

    printf("\n");
    
    printAll(sm);
    deleteFile(sm, "name2", "192.168.0.2", 5552, ANY);
    printf("\n");
    printAll(sm);
    
    printf("\n");printf("\n");
    
    addFileOnTracker(sm, "name5", "md55", "192.168.0.3", 5553);
    addFileOnTracker(sm, "name6", "md56", "192.168.0.3", 5553);
    printAll(sm);

    printf("\n");printf("\n");
    printClient(sm, "192.168.0.3", 5553);
    
    char*      key_2    = new char[16];
    strcpy(key_2, "192.168.0.3");
    deleteClient(sm, key_2, 5553);
    delete key_2;
    printf("<<<\n");
    printClient(sm, "192.168.0.3", 5553);
    printf(">>>\n");
    printAll(sm);
    
    addFileOnTracker(sm, "name4", "md60", "192.168.0.8", 5558);
    addFileOnTracker(sm, "name4", "md54", "192.168.0.9", 5559);
    
    
    printf("\n");
    printAll(sm);
    printf("\n");
    

    int  result2 = search(sm, "name4", first);
    if(result2 == 0) {
        printf("no se encontaron resultados.");
    }
    printf("\n");
    
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>, size: %d\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort(),
                first->current->getSize()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }
    
    addFileOnTracker(sm, "name4", "md60", "192.168.0.9", 5559);
    printf("\n");
    
    int  result3 = search(sm, "name4", first);    
    if(result3 == 0) {
        printf("no se encontaron resultados.");
    }
    printf("\n");
    
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>, md5: %s\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort(),
                first->current->getMD5()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }
    
    addFileOnTracker(sm, "name11", "md61", "192.168.0.9", 5559);
    addFileOnTracker(sm, "name12", "md62", "192.168.0.8", 5559);
    addFileOnTracker(sm, "name13", "md63", "192.168.0.9", 5559);
    printf("\n");
    showClientShares(sm, "192.168.0.9", 5559, first);

    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>, md5: %s\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort(),
                first->current->getMD5()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }  
    deleteHash(sm);
       
    
    printf("\n");    printf("\n");
    printf("\n");    printf("SHARED\n");
    sm = newHash(1000);
    addFileOnClient(sm, "name11", 100, "192.168.0.9", 5559, UPLOADING);
    addFileOnClient(sm, "name12", 100, "192.168.0.9", 5559, SHARED);
    addFileOnClient(sm, "name13", 100, "192.168.0.9", 5559, DOWNLOADING);
    addFileOnClient(sm, "name14", 100, "192.168.0.9", 5559, SHARED);        
      
    showClientShares(sm, "192.168.0.9", 5559, first);
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }
    
    printf("DOWNLOADS\n");    
    showClientDownloads(sm, "192.168.0.9", 5559, first);
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }
    
    printf("UPLOADS\n");            
    showClientUploads(sm, "192.168.0.9", 5559, first);
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort()
        );
        free(first);        
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }                 
    
    finishUpload(sm, "name11", "192.168.0.9", 5559);
    printf("UPLOADS2\n");    
    printf("<<<");
    showClientUploads(sm, "192.168.0.9", 5559, first);
    target = first->current;
    while (target != NULL) {
        next = first->next;
        printf("fileName: %s, fileOwner: <%s:%d>\n", 
                first->current->getName(), 
                first->current->getIP(), 
                first->current->getPort()
        );
        free(first);
        if(next == NULL) {
            target = NULL;
        }
        else {
            target = next->current;
        }
        first = next;
    }
    printf(">>>");
    printAll(sm);
    deleteHash(sm);
}

