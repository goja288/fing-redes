#include <string>
#include "strmap.h"
#include <stdio.h>

typedef struct Pair    Pair;
typedef struct Bucket  Bucket;

struct Pairs {
    Pair  *current;
    Pairs *next;
};

struct Pair {
    char      *key;
    fileEntry *entry;
    Pair      *next;
};

struct Bucket {
    unsigned int count;
    Pair        *pairs;
};

struct StrMap {
    unsigned int count;
    Bucket      *buckets;
};

static Pairs        *getPairs(Bucket *bucket, const char *fileName, const char *ip, unsigned int port, unsigned int &index, State state);
static unsigned long hash(const char *str);

StrMap * newHash(unsigned int capacity)
{
	StrMap *map;	
	map          = new StrMap();
	map->count   = capacity;
	map->buckets = new Bucket[map->count * sizeof(Bucket)];
	memset(map->buckets, 0, map->count * sizeof(Bucket));        
	return map;
}

void deleteHash(StrMap *map)
{
	unsigned int i, n;
	Bucket       *bucket;
	Pair         *pair, *aux;

	if (map == NULL) {
		return;
	}
	n      = map->count;        
	bucket = map->buckets;
	i      = 0;
	while (i < n) {
		pair = bucket->pairs;
		while(pair != NULL) {
                        delete pair->entry;
                        aux = pair->next;                        
                        delete pair;
                        pair = aux;
		}
		bucket++;
		i++;
	}
        delete [] map->buckets;
        delete map;
}

void deletePair(Bucket *bucket, Pair *pair, unsigned int entryIndex)
{
    Pair *prev, *next;
    next = pair->next;
    prev = bucket->pairs;
    if(entryIndex == 0) {
        bucket->pairs = next;
    }
    else {
        for(unsigned int i = 1; i < entryIndex; i++) {
            prev = prev->next;
        }
        prev->next = next;
    }        
    delete pair->entry;  
    delete pair;
    bucket->count--;  
}

int finishUpload(StrMap *map, const char *fileName, const char *ip, unsigned int port)
{
        return deleteFile(map, fileName, ip, port, UPLOADING);
}

int finishDownload(StrMap *map, const char *fileName, const char *ip, unsigned int port)
{
        return deleteFile(map, fileName, ip, port, DOWNLOADING);    
}

int deleteFile(StrMap *map, const char *fileName, const char *ip, unsigned int port, State state)
{
        unsigned int index, entryIndex;
	Bucket       *bucket;
	Pair         *pair;
        Pairs        *first, *next;

	if (map == NULL || fileName == NULL || ip == NULL) {
		return 0;
	}
        
	index  = hash(fileName) % map->count;        
	bucket = &(map->buckets[index]);        
        
	first  = getPairs(bucket, fileName, ip, port, entryIndex, state);
        pair   = first->current;
        while(first != NULL) {
            next  = first->next;
            delete first;
            first = next;
        }
        if (pair == NULL) {
                return 0;
        }
	deletePair(bucket, pair, entryIndex);
	return 1;
}

int get(const StrMap *map, const char *fileName, const char *ip, unsigned int port, Entries* &entries)
{
	unsigned int index, _;
	Bucket       *bucket;
	Pair         *pair;
        Pairs        *firstR, *nextR;
        Entries      *firstE, *currE;            
	if (map == NULL || fileName == NULL) {
		return 0;
	}                
        currE          = new Entries();    
        currE->current = NULL;
        currE->next    = NULL;
        firstE         = currE;
	index          = hash(fileName) % map->count;        
	bucket         = &(map->buckets[index]);
	firstR         = getPairs(bucket, fileName, ip, port, _, ANY);
        while(firstR != NULL) {
            nextR  = firstR->next;
            pair   = firstR->current;
            if(currE->current != NULL) {                
                currE->next    = new Entries();    
                currE          = currE->next;
                currE->current = pair->entry;
                currE->next    = NULL;
            }
            else {
                currE->current = pair->entry;
            }
            delete firstR;
            firstR = nextR;
        }
        entries = firstE;
        return 1;
}

int search(const StrMap *map, std::string fileName, Entries* &entries)
{
    return get(map, fileName.c_str(), NULL, 0, entries);
}

int getFile(const StrMap *map, const char *fileName, const char *ip, unsigned int port, fileEntry* &fileEntry)
{
    Entries *first, *next;
    if (get(map, fileName, ip, port, first) == 0) {
        fileEntry = NULL;
        return 0;
    }
    while(first != NULL) {
        next      = first->next;
        fileEntry = first->current;
        delete first;
        first     = next;
    }
    return 1;
}

bool exists(const StrMap *map, const char *fileName)
{
    unsigned int index, _;
    Bucket       *bucket;
    Pairs        *first, *next;
    bool         result;
    if (map == NULL || fileName == NULL) {
            return 0;
    }
    index  = hash(fileName) % map->count;
    bucket = &(map->buckets[index]);
    first  = getPairs(bucket, fileName, NULL, 0, _, ANY);
    result = (first->current != NULL);
    while(first != NULL) {
        next  = first->next;
        delete first;
        first = next;
    }
    return result;
}

int addFileOnClient(StrMap *map, std::string name, std::string md5, unsigned int size, std::string ip, unsigned int port, State state)
{
    	unsigned int index;
	Bucket       *bucket;
	Pair         *tmp_pairs, *current;
        fileEntry    *entry;        
        bool         same;
	if (map == NULL || name.length() == 0 || ip.length() == 0) {
                return 0;
	};        
        same             = false;        
	index            = hash(name.c_str()) % map->count;
	bucket           = &(map->buckets[index]);             
        current          = bucket->pairs;
        if(current != NULL) {
            same    = (
                            (strcmp(current->entry->getName(), name.c_str()) ==     0) &&
                            (strcmp(current->entry->getIP(),     ip.c_str()) ==     0) &&
                                   (current->entry->getState()               == state) &&
                                   (current->entry->getPort()                ==  port)
                       );
            while(current->next != NULL && !same) {
                current = current->next;
                same    = (
                            (strcmp(current->entry->getName(), name.c_str()) ==     0) &&
                            (strcmp(current->entry->getIP(),     ip.c_str()) ==     0) &&
                            (current->entry->getState()                      == state) &&
                                   (current->entry->getPort()                ==  port)
                       );
            }
            if(!same) {
                char* nameChr    = new char[512];
                char* ipChr      = new char[16];
                char* md5Chr     = new char[33];
                strcpy(nameChr, name.c_str()); 
                strcpy(ipChr,     ip.c_str());
                strcpy(md5Chr,   md5.c_str()); 
                entry            = new fileEntry(nameChr, md5Chr, size, ipChr, port, state);
                tmp_pairs        = new Pair();
                tmp_pairs->key   = entry->getName();
                tmp_pairs->entry = entry;
                tmp_pairs->next  = NULL;
                current->next    = tmp_pairs;
                bucket->count++;
            }
            else {
                current->entry->setSize(size);
                current->entry->setState(state);
            }
        }
        else {                
                char* nameChr    = new char[512];
                char* ipChr      = new char[16];
                char* md5Chr     = new char[33];
                strcpy(nameChr, name.c_str()); 
                strcpy(ipChr,     ip.c_str());
                strcpy(md5Chr,   md5.c_str());
                entry            = new fileEntry(nameChr, md5Chr, size, ipChr, port, state);
                tmp_pairs        = new Pair();
                tmp_pairs->key   = entry->getName();
                tmp_pairs->entry = entry;
                tmp_pairs->next  = NULL;
                bucket->pairs    = tmp_pairs;
                bucket->count++;
	}
    return 1;
}

int addFileOnTracker(StrMap *map, std::string name, std::string md5, std::string ip, unsigned int port)
{
	unsigned int index;
	Bucket       *bucket;
	Pair         *tmp_pairs, *current;
        fileEntry    *entry;        
        bool         same;
	if (map == NULL || name.length() == 0 || ip.length() == 0) {
                return 0;
	};        
        same             = false;        
	index            = hash(name.c_str()) % map->count;
	bucket           = &(map->buckets[index]);             
        current          = bucket->pairs;
        char* md5Chr = new char[512];
        if(current != NULL) {
            same    = (
                            (strcmp(current->entry->getName(), name.c_str()) ==    0) &&
                            (strcmp(current->entry->getIP(),     ip.c_str()) ==    0) &&
                            
                                   (current->entry->getPort()                == port)
                       );
            while(current->next != NULL && !same) {
                current = current->next;
                same    = (
                            (strcmp(current->entry->getName(), name.c_str()) ==    0) &&
                            (strcmp(current->entry->getIP(),     ip.c_str()) ==    0) &&
                                   (current->entry->getPort()                == port)
                       );
            }
            strcpy(md5Chr, md5.c_str());
            if(!same) {
                char* nameChr    = new char[50];
                char* ipChr      = new char[16];
                strcpy(nameChr, name.c_str()); 
                strcpy(ipChr,     ip.c_str());
                entry            = new fileEntry(nameChr, md5Chr, ipChr, port);
                tmp_pairs        = new Pair();
                tmp_pairs->key   = entry->getName();
                tmp_pairs->entry = entry;
                tmp_pairs->next  = NULL;
                current->next    = tmp_pairs;
                bucket->count++;
            }
            else {
                delete current->entry->getMD5();
                current->entry->setMD5(md5Chr);
            }
        }
        else {
                char* nameChr    = new char[50];
                char* ipChr      = new char[16];
                strcpy(md5Chr, md5.c_str());
                strcpy(nameChr, name.c_str()); 
                strcpy(ipChr,     ip.c_str());
                entry            = new fileEntry(nameChr, md5Chr, ipChr, port);
                tmp_pairs        = new Pair();
                tmp_pairs->key   = entry->getName();
                tmp_pairs->entry = entry;
                tmp_pairs->next  = NULL;
                bucket->pairs    = tmp_pairs;
                bucket->count++;
	}
    return 1;
}

int getTotalCount(const StrMap *map)
{
	unsigned int i, j, n, m;
	unsigned int count;
	Bucket       *bucket;
	Pair         *pair;

	if (map == NULL) {
		return 0;
	}
	bucket = map->buckets;
	n      = map->count;
	i      = 0;
	count  = 0;
	while (i < n) {
		pair = bucket->pairs;
		m    = bucket->count;
		j    = 0;
		while (j < m) {
			count++;
			pair++;
			j++;
		}
		bucket++;
		i++;
	}
	return count;
}

int sm_enum(StrMap *map, const char *ip, unsigned int port, sm_enum_func enum_func, Entries* &entries, bool collect, State state)
{
	unsigned int i, n;
	Bucket       *bucket;
	Pair         *pair, *next;
        fileEntry    *currF;
        Entries      *currE;
        if(collect) {
            entries          = new Entries();    
            entries->current = NULL;
            entries->next    = NULL;            
            currE            = entries;
        }
	if (map == NULL) {
		return 0;
	}
	if (enum_func == NULL) {
		return 0;
	}
	bucket = map->buckets;
	n      = map->count;
	i      = 0;
	while (i < n) {
		pair = bucket->pairs;
		while (pair != NULL) {
                        next = pair->next;                        
			enum_func(ip, port, bucket, pair, currF);                        			
                        if(collect && (currF != NULL) && ((state == ANY) || (currF->getState() == state))) {                            
                            if(currE->current != NULL) {                
                                currE->next    = new Entries();    
                                currE          = currE->next;
                            }
                            currE->current = currF;
                            currE->next    = NULL;
                        }
                        pair = next;
		}
		bucket++;
		i++;
	}
	return 1;
}

static Pairs * getPairs(Bucket *bucket, const char *fileName, const char *ip, unsigned int port, unsigned int &index, State state)
{
    unsigned int i, n;
    Pair         *pair;
    Pairs        *firstResult, *currentResult;
    char*        targetMD5;
    
    currentResult          = new Pairs();    
    currentResult->current = NULL;
    currentResult->next    = NULL;
    firstResult            = currentResult;
    n                      = bucket->count;    
    bool fullEval          = (ip != NULL);
    pair                   = bucket->pairs;
    i                      = 0;
    while (i < n) {
            if (pair->key != NULL && pair->entry != NULL) {
                    if (strcmp(pair->key, fileName) == 0) {
                        if(
                                (!fullEval) || 
                                ((strcmp(pair->entry->getIP(), ip) ==    0) && 
                                        (pair->entry->getPort()    == port) &&
                                       ((state == ANY) ||
                                        (pair->entry->getState() == state))
                                )
                          ) {
                            index = i;
                            if((currentResult->current != NULL) && (strcmp(targetMD5, pair->entry->getMD5()) == 0)) {
                                currentResult->next    = new Pairs();    
                                currentResult          = currentResult->next;
                                currentResult->current = pair;
                                currentResult->next    = NULL;                                
                            }
                            else {
                                currentResult->current = pair;
                                targetMD5              = pair->entry->getMD5();
                                if(fullEval) {
                                    return firstResult;
                                }
                            }
                        }
                    }
            }
            pair = pair->next;
            i++;
    }
    return firstResult;
}

static unsigned long hash(const char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++)) {
            hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

static void printAllIter(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry)
{
    fileEntry = NULL;
    printf("%s, <%s:%d>\n", currentPair->entry->getName(), currentPair->entry->getIP(), currentPair->entry->getPort());
}

static void printClientIter(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry)
{
    fileEntry = NULL;
    if ((strcmp(currentPair->entry->getIP(), ip) == 0) && (currentPair->entry->getPort() == port)) {
        printf("fileName: %s\n", currentPair->entry->getName());
    }
}

static void deleteClientIter(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry)
{
    unsigned int entryIndex;
    Pairs        *first, *next;
    fileEntry = NULL;
    if ((strcmp(currentPair->entry->getIP(), ip) == 0) && (currentPair->entry->getPort() == port)) {
        first = getPairs(bucket, currentPair->entry->getName(), ip, port, entryIndex, ANY);
        while(first != NULL) {
            next = first->next;
            delete first;
            first = next;
        }
        deletePair(bucket, currentPair, entryIndex);
    }
}

static void showClientIter(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry)
{
    fileEntry = NULL;
    if ((strcmp(currentPair->entry->getIP(), ip) == 0) && (currentPair->entry->getPort() == port)) {
        fileEntry = currentPair->entry;
    }
}

static void showAllIter(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry)
{
    fileEntry = currentPair->entry;    
}

void printAll(StrMap *map)
{
    Entries *entries;
    sm_enum(map, NULL, 0, printAllIter, entries, false, ANY);
}

void printClient(StrMap *map, const char *ip, unsigned int port)
{
    Entries *entries;
    sm_enum(map, ip, port, printClientIter, entries, false, ANY);
}

void deleteClient(StrMap *map, const char *ip, unsigned int port)
{
    Entries *entries;
    sm_enum(map, ip, port, deleteClientIter, entries, false, ANY);
}

void showClientShares(StrMap *map, const char *ip, unsigned int port, Entries* &entries)
{
    sm_enum(map, ip, port, showClientIter, entries, true, SHARED);
}

void showClientDownloads(StrMap *map, Entries* &entries)
{
    sm_enum(map, NULL, 0, showAllIter, entries, true, DOWNLOADING);
}

void showClientUploads(StrMap *map, Entries* &entries)
{
    sm_enum(map, NULL, 0, showAllIter, entries, true, UPLOADING);
}
