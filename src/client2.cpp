// COMPILACIÓN
// g++ client2.cpp -o client2 -lcrypto -lssl -g
// 
// EJECUCIÓN
// ./client2 -tIP 127.0.0.1 -tPort 5556 -admPort 6666 -listPort 5555 -SFpath /home/alejandro/Descargas/

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <netdb.h>
#include <iostream>
#include <csignal>

#include <unistd.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include <algorithm>

#include "strmap.h"
#include "fileEntry.h"
#include "client.h"

#define TRACKER_IP "127.0.0.1"          // Tracker IP por defecto
#define TRACKER_PORT "5556"		// Tracker Port por defecto
#define CLIENT_IP "127.0.0.1"
#define CLIENT_PORT 6666		// Client Port por defecto
#define LISTEN_PORT 5555                // Puerto de escucha de cliente 

#define MAX_MSG_SIZE 1024
#define MAX_QUEUE 10
#define TAM_BUFFER 262144

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option) {
    return std::find(begin, end, option) != end;
}

int listen_terminal_socket;
int listen_clients_socket;
int new_terminal_socket;
int new_socket;

void exitCTRL(int sig) {
    close(listen_terminal_socket);
    close(listen_clients_socket);
    close(new_terminal_socket);
    close(new_socket);
    exit(1);
}

int main(int argc, char *argv[]) 
{
    // Definicion de estructura
    StrMap*  map_archivos;
    StrMap*  map_clientes;
    
    map_archivos                = newHash(500);
    map_clientes                = newHash(100);
    
    struct archivos s_archivos_subida[200];
    struct archivos s_archivos_baja[200];
    for (int a=0;a<200;a++) {
        s_archivos_baja[a].archivo              = NULL;
        s_archivos_baja[a].archivo_md5          = NULL;
        s_archivos_subida[a].archivo            = NULL;
        s_archivos_subida[a].archivo_md5        = NULL;
    }
    
    listen_terminal_socket      = -1;
    listen_clients_socket       = -1;
    new_terminal_socket         = -1;
    new_socket                  =  1;
    int error                   =  1;
    int on                      =  1;
    struct sockaddr_in  address;
    struct pollfd fds[200];
    int    nfds                 = 0;
    int current_size            = 0;
    int timeout                 = 0;
    int quit                    = 0;
    int i,j;
    int shutdown_client         = 0;
    int close_socket            = 0;
    int clean_structure         = 0;
   // char* buffer;
    int cant_bytes;
    int terminal_closed         = 1;
    int namelen;
 
    
/******************************************************************************/   

/******************************************************************************/        
        
    signal(SIGINT, exitCTRL);
    
    printf("\n");
    
    const char* host = TRACKER_IP;
    if(cmdOptionExists(argv, argv+argc, "-tIP")) {
        host = getCmdOption(argv, argv + argc, "-tIP");
    }
    else {
        host = TRACKER_IP; 
    }
    printf("Tracker IP: %s\n",host);

    //Se pasa por parámetro un Server Port
    const char* port_tracker;
    if(cmdOptionExists(argv, argv+argc, "-tPort")) {
        port_tracker = getCmdOption(argv, argv + argc, "-tPort");
    } 
    else { 
        port_tracker = TRACKER_PORT; 
    }
    printf("Port IP: %s\n",port_tracker);


    
    //Se pasa por parámetro un Server Port
    char* shared_folder_path = new char[MAX_MSG_SIZE];
    bzero(shared_folder_path,MAX_MSG_SIZE);
    if(cmdOptionExists(argv, argv+argc, "-SFpath")) {
        shared_folder_path = getCmdOption(argv, argv + argc, "-SFpath");
        strcat(shared_folder_path, "/");
    } 
    else {
        getcwd(shared_folder_path, MAX_MSG_SIZE);
        strcat(shared_folder_path, "/");
    }
//    printf("Shared Folder Path: %s\n",shared_folder_path); // SE IMPRIME MAS ABAJO, Y SE LE SUMA EL DIRECTORIO sfXXXX/
    
    // Creo los socketes de bienvenida
    listen_terminal_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_terminal_socket < 0) { perror("[ERROR] socket()"); exit(-1); } // bye!!
    
    listen_clients_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_clients_socket < 0) { perror("[ERROR] socket()"); exit(-1); } // bye!!
  	
    // Les digo a los socketes para que puedan ser reutilizables
    error = setsockopt(listen_terminal_socket, SOL_SOCKET,  SO_REUSEADDR,
            (char *)&on, sizeof(on));
    if (error < 0)  { perror("[ERROR] setsockopt()"); close(listen_terminal_socket); exit(-1); } // chau!!
    
    error = setsockopt(listen_clients_socket, SOL_SOCKET,  SO_REUSEADDR,
            (char *)&on, sizeof(on));
    if (error < 0)  { perror("[ERROR] setsockopt()"); close(listen_clients_socket); exit(-1); } // chau!!
    
    // y ahora les digo que sean no bloqueantes
    error = ioctl(listen_terminal_socket, FIONBIO, (char *)&on);
    if (error < 0) { perror("[ERROR] ioctl()"); close(listen_terminal_socket); exit(-1); }// adieu !!
    
    error = ioctl(listen_clients_socket, FIONBIO, (char *)&on);
    if (error < 0) { perror("[ERROR] ioctl()"); close(listen_clients_socket); exit(-1); }// adieu !!
    
    //Se pasa por parámetro un Client Port
    int client_port;
    if(cmdOptionExists(argv, argv+argc, "-admPort")) {
        client_port = atoi(getCmdOption(argv, argv + argc, "-admPort"));
    }
    else {
        client_port = CLIENT_PORT;
    }
    printf("Administration Port: %i\n",client_port);
    
    // Hago los binding
    memset(&address, 0, sizeof(address));
    address.sin_family      = AF_INET;
    address.sin_addr.s_addr = inet_addr(CLIENT_IP);
    address.sin_port        = htons(client_port);
    error = bind(listen_terminal_socket, (struct sockaddr *)&address, sizeof(address));
    while (error < 0) {
        printf("[ERROR] Puerto ocupado\n");
        address.sin_port        = 0;
        error = bind(listen_terminal_socket, (struct sockaddr *)&address, sizeof(address));
        namelen = sizeof(address);
        if (getsockname(listen_terminal_socket, (struct sockaddr *) &address,(socklen_t*) &namelen) < 0) {
            printf("getsockname()\n");
            exit(-1);
        }
        printf("Administration Port asignado: %d\n", ntohs(address.sin_port));
    }
    
    //Cambio el puerto de escucha
    int listen_port;
    if(cmdOptionExists(argv, argv+argc, "-listPort")) {
        listen_port = atoi(getCmdOption(argv, argv + argc, "-listPort"));
    }
    else {
        listen_port = LISTEN_PORT;
    }
    printf("Listen Port: %i\n",listen_port);
    
    memset(&address, 0, sizeof(address));
    address.sin_family      = AF_INET;
    address.sin_addr.s_addr = inet_addr(CLIENT_IP);
    address.sin_port        = htons(listen_port);
    error = bind(listen_clients_socket, (struct sockaddr *)&address, sizeof(address));
    while (error < 0) {
        printf("[ERROR] Puerto ocupado\n");
        address.sin_port        = 0;
        error = bind(listen_clients_socket, (struct sockaddr *)&address, sizeof(address));
        namelen = sizeof(address);
        if (getsockname(listen_clients_socket, (struct sockaddr *) &address,(socklen_t*) &namelen) < 0) {
            printf("getsockname()\n");
            exit(-1);
        }
        printf("Listen Port asignado: %d\n", ntohs(address.sin_port));
        listen_port = ntohs(address.sin_port);
    }
    char* shared_folder_aux = new char[MAX_MSG_SIZE];
    bzero(shared_folder_aux,MAX_MSG_SIZE);
    strcat(shared_folder_aux,shared_folder_path);
    strcat(shared_folder_aux,"sf");
    char* listen_port_char = new char[6];
    bzero(listen_port_char,6);
    sprintf(listen_port_char,"%i",listen_port);
    strcat(shared_folder_aux,listen_port_char);
    strcat(shared_folder_aux,"/");
    shared_folder_path = shared_folder_aux;
    char* re_aux = new char[MAX_MSG_SIZE];
    bzero(re_aux,MAX_MSG_SIZE);
    sprintf(re_aux,"mkdir \"%s\"",shared_folder_aux);
    system(re_aux);
//    printf("SF_PATH:-%s-\n",shared_folder_path);
//    printf("SF_AUX:-%s-\n",shared_folder_aux);
//    printf("RE_AUX:-%s-\n",re_aux);
    
    printf("\n");

    // Me pongo a escuchar [:o
    error = listen(listen_terminal_socket, MAX_QUEUE);
    if (error < 0) { perror("[ERROR] listen()"); close(listen_terminal_socket); exit(-1); }
    
    error = listen(listen_clients_socket, MAX_QUEUE);
    if (error < 0) { perror("[ERROR] listen()"); close(listen_clients_socket); exit(-1); }

/**********************************************/    
/* Fin de definicion de Sockets de "escucha"  */    
/**********************************************/    
    
  
    // Inicializo la estructura de poll y agrego los socketes de bienvenida 
    memset(fds, 0 , sizeof(fds));

    fds[nfds].fd = listen_terminal_socket;
    fds[nfds].events = POLLIN | POLLRDHUP; // Eventos que escucha
    nfds++;
    
    fds[nfds].fd = listen_clients_socket;
    fds[nfds].events = POLLIN | POLLRDHUP; // Eventos que escucha
    nfds++;
    
    /*****************************************/
    /* Me conecto al viejo y querido tracker */
    /*****************************************/
    
    int client_tracker_socket = socket(AF_INET, SOCK_STREAM, 0); 
    
    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(TRACKER_IP, port_tracker, &hints, &res);

    // Me conecto al tracker 
    error = connect(client_tracker_socket, res->ai_addr, res->ai_addrlen);
    if (error < 0) { perror("[ERROR] connect()"); close(client_tracker_socket); exit(-1); }
    
    bool ok = newClient(listen_port,(char*) CLIENT_IP,client_tracker_socket,MAX_MSG_SIZE);
    if (!ok) { 
        printf("[ERROR] No se pudo dar de alta el cliente\n");
        exit(1); 
    }
    
    /**************************************************/
    /* Fin de la definicion de la conexion al tracker */
    /**************************************************/
    

    //timeout = (1 * 60 * 1000); // @FIXME modificar cuando este terminado
    timeout = (-1); // Hasta el final de los tiempos
    
/******************************************************************************/    
    
    
    // Aca empieza lo divertido 
    do {
        error = poll(fds, nfds, timeout);
        if (error < 0) { perror("[ERROR] poll()"); break; } // hago un break para poder cerrar todo antes de irme
 	if (error == 0) { printf("[ERROR] se acabo el tiempo de poll() \n"); break; } // idem
        
        // Analizamos a poll alias Paul
        current_size = nfds;
        //if (current_size == 5) {
        //        printf("\n*****ACTUAL SIZE**** %i **\n", current_size);
        //}
        
        for (i = 0; i < current_size; i++) {
            //print_pollfd(fds);
            
            // Busco por todo Paul hasta que algun socket tenga un evento
            if(fds[i].revents == 0) {
                continue;
            }
            
            // Soy el que escucha
            if (fds[i].fd == listen_terminal_socket) {
                
                // Procedo a aceptar nuevas conexiones que estaban a la espera
                //do {
                    
                    // Salgo del loop si da el error EWOULDBLOCK que indicaria 
                    // que no tenemos mas 
                    new_socket = accept(listen_terminal_socket, NULL, NULL);
                    if (new_socket < 0) {
                        if (errno != EWOULDBLOCK) {
                                perror(" [ERROR] accept() de nuevas conexiones");
                                shutdown_client = 1;
                        }
                        break; // LLEGA POR SER NO BLOQUEANTE
                    }
                    else {
                        // Agrego la conexion a la estructura del poll
                        if (terminal_closed) {
                            new_terminal_socket = new_socket;
                            fds[nfds].fd = new_terminal_socket;
                            fds[nfds].events = POLLIN | POLLRDHUP; // Eventos que escucha
                            nfds++;
                            terminal_closed = 0;
                        }
                        else {
                            char * msg_to_terminal = new char[128];
                            bzero(msg_to_terminal,128);
                            strcat(msg_to_terminal,"\n[ERROR] Existe otra consola de administracion corriendo\n\n");
                            sendMsgToSocket(msg_to_terminal,new_socket,0);
                            close(new_socket);
                            delete [] msg_to_terminal;
                        }
                    }
                    sendMsgToSocket((char*) "\n\n", new_terminal_socket, 0);
                //} while (new_terminal_socket != -1); // si es distino de -1 quiere decir que acepto una conexion y le asigna numero correlativos
                
            }
            else if (fds[i].fd == listen_clients_socket) {
                // Procedo a aceptar nuevas conexiones que estaban a la espera
                // a las cuales voy a enviar archivos
                do {
                    // Salgo del loop si da el error EWOULDBLOCK que indicaria 
                    // que no tenemos mas 
                    new_socket = accept(listen_clients_socket, NULL, NULL);
                    if (new_socket < 0) {
                        if (errno != EWOULDBLOCK) {
                                perror(" [ERROR] accept() de nuevas conexiones");
                                shutdown_client = 1;
                        }
                        break; // LLEGA POR SER NO BLOQUEANTE
                    }
                    /************************/
                    /** Nombre del archivo **/
                    /************************/
                    char * pedidoCliente = new char[512];
                    bzero(pedidoCliente,512);                            
                    cant_bytes           = recv(new_socket, pedidoCliente, 512, 0);                    
                    
                    if (cant_bytes < 0) { // @FIXME ver si no tendria que ser <=
                        if (errno != EWOULDBLOCK) {
                            perror("[ERROR] recv() ");
                            close(new_socket);
                        }
                        break; // Me voy porque hubo un error o porque no habia nada 
                    }
                    else {
                        struct sockaddr_in peer;
                        int    peer_len;
                        char  *ipTarget         = new char[16];
                        peer_len                = sizeof(peer);
                        getpeername(new_socket, (struct sockaddr *)&peer, (socklen_t *) &peer_len); // @FIXME no esoty seguro de la forzada de peer_len es un int realmente
                        sprintf(ipTarget, "%s", inet_ntoa(peer.sin_addr));
                        unsigned int portTarget = ntohs(peer.sin_port);
                        
                        //pedidoCliente[cant_bytes-1]='\0'; // Borro el espacio
                        pedidoCliente[cant_bytes-1]='\0'; // Borro el \n
                        char * filename = new char[512];
                        bzero(filename,512);
                        char* nombre_archivo = getFileName(filename,pedidoCliente,shared_folder_path);
                        
                        
                        
                        FILE* archivo = fopen(filename, "rb"); // abro el archivo de solo lectura.
                        delete [] filename;
                        if (archivo == NULL) {
                            printf("[ERROR] El archivo no pudo ser abierto");
                            close(new_socket);
                            break;
                        }
                        
                        printf("Socket Asignado al Cliente Downloader: %i\n", new_socket);        
                        // Agrego la conexion a la estructura del poll
                        fds[nfds].fd      = new_socket;
                        fds[nfds].events  = POLLRDHUP | POLLOUT; // Eventos que escucha
                        nfds++;                        
                        s_archivos_subida[new_socket].archivo = archivo;
                        char *socket_char = new char[3];
                        sprintf(socket_char, "%d", new_socket);
                        
                        /*for (int b=0;b<200;b++) {
                                if (s_archivos_baja[b].archivo      != NULL) {
                                    printf("Hay un archivo bajandose en %i\n",b);
                                }
                                if (s_archivos_subida[b].archivo      != NULL) {
                                    printf("Hay un archivo subiendose en %i\n",b);
                                }
                            }**/
                        //printf(">> %s::%s::%s::%i::\n",socket_char,filename,ipTarget,portTarget);
                        
                        addFileOnTracker(map_clientes, socket_char, nombre_archivo, ipTarget, portTarget);
                        addFileOnClient(map_archivos, nombre_archivo, "", 0, ipTarget, portTarget, UPLOADING);
//                        delete [] ipTarget;
                        delete [] ipTarget;
                        delete [] socket_char;
                        delete [] nombre_archivo;
                    }
                    delete [] pedidoCliente;
                    
                } while (new_socket != -1); // si es distino de -1 quiere decir que acepto una conexion y le asigna numero correlativos
            } 
            else { // No soy el que escucha, con lo cual procedemos a analizar el poll
                
                do { 
                    
                    
                    
                    if (fds[i].fd == new_terminal_socket) {
                        // Primero que nada me fijo si esta 
                        if (fds[i].revents & POLLRDHUP) {
                                printf("\tSe fue la consola !!!\n");
                                terminal_closed = 1;
                                close_socket = 1; 
                                break;
                        }
                        if (fds[i].revents & POLLIN) {
                            char * bufferComandos = new char[512];
                            bzero(bufferComandos,512);
                            
                            cant_bytes = recv(fds[i].fd, bufferComandos, 512, 0);
                            if (cant_bytes < 0) { // @FIXME ver si no tendria que ser <=
                                if (errno != EWOULDBLOCK) {
                                        perror("[ERROR] recv() ");
                                        close_socket = 1;
                                 }
                                break; // Me voy porque hubo un error o porque no habia nada 
                            
                            }
                            
                            bufferComandos[cant_bytes-1]='\0'; // Borro el espacio
                            bufferComandos[cant_bytes-2]='\0'; // Borro el \n
                            
                            
                            quit = clientAdministration(new_terminal_socket,client_tracker_socket,bufferComandos,fds,nfds,s_archivos_baja,map_archivos, map_clientes,shared_folder_path,listen_port);
//                            printf("quit %i\n",quit);
                            /*for (int b=0;b<200;b++) {
                                if (s_archivos_baja[b].archivo      != NULL) {
                                    printf("--Hay un archivo bajandose en %i\n",b);
                                }
                                if (s_archivos_subida[b].archivo      != NULL) {
                                    printf("--Hay un archivo subiendose en %i\n",b);
                                }
                            }*/

                            if (quit == 1) {
                                shutdown_client = 1;
                            }
                            else if (quit == 2) {
                                printf("[ERROR] No se pudo crear la conexion\n");
                            }
                            
                            delete [] bufferComandos;
                            break;
                            
                        }
                        
                        
                    }
                    
                    // Si esta me fijo si me esta queriendo enviar algo (un archivo)
                    // Primero que nada me fijo si esta 
                        if (fds[i].revents & POLLRDHUP) {
                                printf("\tSe nos fue un cliente !!!\n");
                                
                                close_socket = 1; 
                                break;
                        }
                    
                        if (fds[i].revents & POLLIN) {
                            char         buffer2[TAM_BUFFER];
                            Entries     *first, *next;
                            fileEntry   *target;
                            char        *ipSource, *fileName, *socketChr;
                            unsigned int portSource;
                            socketChr = new char[3];
                            sprintf(socketChr, "%d", fds[i].fd);
                            std::string socketStr;
                            socketStr.assign(socketChr);
                            search(map_clientes, socketStr, first);
                            target = first->current;
                            
                            while(target != NULL) {
                                next       = first->next;
                                ipSource   = target->getIP();
                                portSource = target->getPort();
                                fileName   = target->getMD5(); //tricky hack
                                delete first;
                                if(next == NULL) {
                                    target = NULL;
                                }
                                else {
                                        target = next->current;
                                }
                                first = next;
                            }
                            //printf(">> %s::%s::%s::%s\n",fileName, ipSource ,portSource, Target);
                            getFile(map_archivos, fileName, ipSource, portSource, target);
                            
                            int bytes_leidos = recv(fds[i].fd, buffer2, sizeof(buffer2), 0);
                            if (bytes_leidos < 0) { // @FIXME ver si no tendria que ser <=
                                if (errno != EWOULDBLOCK) {
                                    perror("[ERROR] recv() ");
                                    close_socket = 1;
                                }
                                finishDownload(map_archivos, fileName, ipSource, portSource);
                                fclose(s_archivos_baja[fds[i].fd].archivo);
                                fclose(s_archivos_baja[fds[i].fd].archivo_md5);
                                deleteFile(map_clientes, socketChr, ipSource, portSource, SHARED);
                                close_socket = 1;
                                break; // Me voy porque hubo un error o porque no habia nada 
                            }
                            
                            if (bytes_leidos > 0) {
                                
                                        // @FIXME CHEQUEO DE ERROR DEL WHAT
                                    /*int what = */fwrite(buffer2,1,bytes_leidos,s_archivos_baja[fds[i].fd].archivo);
                                    fflush(s_archivos_baja[fds[i].fd].archivo);
                                    target->setBytesDownloaded(target->getBytesDownloaded() + bytes_leidos);

                            }
                            char *md5_aux2 = new char[33];
                            bzero(md5_aux2,33);
                            // Cambiado para que no abra y cierre el archivo
                            calculoMD5_2(s_archivos_baja[fds[i].fd].archivo_md5,md5_aux2);
                            
                            if (strstr(md5_aux2,target->getMD5()) != NULL) {
                                printf("Termino de descargar\n");
                                
                                char * msg_to_terminal = new char[100];
                                bzero(msg_to_terminal,100);
                                strcat(msg_to_terminal,"Termino de descargar\n");
                                strcat(msg_to_terminal,"-------------------------------------------\n\n");
                                sendMsgToSocket(msg_to_terminal, new_terminal_socket, 0);
                                delete [] msg_to_terminal;
                                
                                finishDownload(map_archivos, fileName, ipSource, portSource);
                                fclose(s_archivos_baja[fds[i].fd].archivo);
                                fclose(s_archivos_baja[fds[i].fd].archivo_md5);
                                deleteFile(map_clientes, socketChr, ipSource, portSource, SHARED);
                                close_socket = 1;
//                                free(socketChr);
                            }
                            delete [] md5_aux2;
                            delete [] socketChr;
                            break;

                        }
                    
                    // Tengo que enviar cosas (archivos)
                    if (fds[i].revents & POLLOUT) {
                        Entries     *first, *next;
                        fileEntry   *target, *source;
                        //fileEntry   *target;
                        char        *ipTarget, *fileName, *socketChr;
                        unsigned int portTarget;
                        socketChr = new char[3];
                        sprintf(socketChr, "%d", fds[i].fd);
                        std::string socketStr;
                        socketStr.assign(socketChr);
                        search(map_clientes, socketStr, first);
                        target = first->current;
                        while(target != NULL) {
                            next       = first->next;
                            ipTarget   = target->getIP();
                            portTarget = target->getPort();
                            fileName   = target->getMD5(); //tricky hack
                            delete first;
                            if(next == NULL) {
                                target = NULL;
                            }
                            else {
                                    target = next->current;
                            }
                            first = next;
                        }
                        getFile(map_archivos, fileName, ipTarget, portTarget, target);
                        //printf("::%s::%s::%i\n", fileName, CLIENT_IP, listen_port);
                        getFile(map_archivos, fileName, CLIENT_IP, listen_port, source);
                        
                        //printf("Tamaño transferido: %i\n",target->getSize());
                        
                        char buffer[TAM_BUFFER]; // Buffer
                        int bytesLeidos;
                        
                        bytesLeidos = fread(buffer,1,sizeof(buffer),s_archivos_subida[fds[i].fd].archivo);
                        if (bytesLeidos > 0) {
                              send(fds[i].fd, buffer, bytesLeidos, 0);
                              target->setBytesUploaded(target->getBytesUploaded() + bytesLeidos);
                              source->setBytesUploaded(source->getBytesUploaded() + bytesLeidos);
                        }
                        else {
                            finishUpload(map_archivos, fileName, ipTarget, portTarget); // @FIXME DESCOMENTAR ver porque hay que
                            fds[i].events = POLLRDHUP;
                            fclose(s_archivos_subida[fds[i].fd].archivo);
                        }
                        delete [] socketChr;
                    }
                    
                    break; // Me voy porque ya procese a este compañero 
                } while(1); // Alguien te va a quebrar antes no vas a estar corriedo 4ever
                if (close_socket == 1) {
//                    printf("Voy a cerrar %i\n",fds[i].fd);
                    close(fds[i].fd);
                    fds[i].fd = -1;
                    clean_structure = 1; // Aviso que hay uno que no esta mas para poder borrarlo
                    close_socket = 0;
                }
               
                
            }
            
        }
        
        // Reordenamos el poll para no tomar aquellos que nos abandonaron
        
        if (clean_structure) {
            clean_structure = 0;
            for (i = 0; i < nfds; i++) {
                if (fds[i].fd == -1) {
                    for(j = i; j < nfds; j++) {
                        fds[j].fd = fds[j+1].fd;
                    }
                    nfds--;
                }
            }
        }
        
		
    } while (!shutdown_client); 
    
    // El ultimo que apague la luz (si queda algun socket abierto lo cierro)
    for (i = 0; i < nfds; i++) {
        if(fds[i].fd >= 0) {
                close(fds[i].fd);
        }
    }
    deleteHash(map_clientes);
    deleteHash(map_archivos);
    freeaddrinfo(res);
    delete [] listen_port_char;
    delete [] shared_folder_path;
    //delete [] shared_folder_clean;

}


// OK
char* calculoMD5(const char* filename, char* res, const char* shared_folder_path) {
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    char* full_filename = new char[MAX_MSG_SIZE];
    bzero(full_filename,MAX_MSG_SIZE);
    strcat(full_filename,shared_folder_path);
    strcat(full_filename,filename);
//    printf("Full_filename: %s\n",full_filename);
    FILE* inFile = fopen (full_filename, "rb");
    MD5_CTX mdContext;
    int bytes;
    unsigned char data[TAM_BUFFER];
    
    if (inFile == NULL) {
//        printf ("%s no se puede abrir.\n", auxi);
        res = (char*) "[ERROR] No existe el archivo\n";
        return res;
    }

    MD5_Init(&mdContext);
    while ((bytes = fread (data, 1, TAM_BUFFER, inFile)) != 0)
        MD5_Update(&mdContext, data, bytes);
    MD5_Final(c,&mdContext);
    char* aux = new char[3];
    
    
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        bzero(aux,3);
//        printf("%02x", c[i]);
        sprintf(aux,"%02x",c[i]);
        strcat(res, aux);
    }
//    printf (" %s\n", filename);
    fclose (inFile);        
    
    delete [] aux;
    delete [] full_filename;
    
    return res;
}

// OK
char* calculoMD5_2(FILE* inFile, char* res) {
    rewind(inFile);
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    MD5_CTX mdContext;
    int bytes;
    unsigned char data[TAM_BUFFER];
    
    if (inFile == NULL) {
        res = (char*) "[ERROR] No existe el archivo\n";
        return res;
    }

    MD5_Init(&mdContext);
    while ((bytes = fread (data, 1, TAM_BUFFER, inFile)) != 0)
        MD5_Update(&mdContext, data, bytes);
    MD5_Final(c,&mdContext);
    char* aux = new char[3];
    
    
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        bzero(aux,3);
        sprintf(aux,"%02x",c[i]);
        strcat(res, aux);
    }
    
    delete [] aux;

    
    return res;
}


// OK
void sendMsgToSocket (char* msg, int socket, int imp) { // Si imp es 0, no imprimo nada

    int msg_size = strlen(msg);
    send(socket, msg, msg_size, 0);    // Envio al Tracker un mensaje
//    int sent_msg_size = send(socket, msg, msg_size, 0);    // Envio al Tracker un mensaje
//    if (imp == 1)
//        printf("Enviado al tracker (%d bytes): \n%s\n\n", sent_msg_size, msg);
//    else if (imp == 2)
//        printf("Enviado a la terminal (%d bytes): \n%s\n", sent_msg_size, msg);
}

// OK
bool recvMsgToSocket (int socket, char** ip, char** port, char** md5) {
    
    bool ok = 1;
    int dato_size       = MAX_MSG_SIZE;
    //void* datoV         = malloc(MAX_MSG_SIZE);
    char* datoV = new char[MAX_MSG_SIZE];
    bzero(datoV,MAX_MSG_SIZE);
    int recv_dato_size = recv(socket, datoV, dato_size, 0);                             
    if (recv_dato_size <= 0) { // O si no envio nada -1 si hubo error
        if (errno != EWOULDBLOCK) {
                perror("[ERROR] recv() ");
                ok = false;
        }
        
    }
    else {
        char* dato = (char*) datoV;
//        printf("Recibido del tracker (%d bytes): \n%s\n\n", recv_dato_size, dato);

        //ANALISIS DE LA RESPUESTA
        if (strcmp(dato,"ok") == 0) {
            printf("[OK] Recibimos un OK");
            //return true;
        }              
        else if (strcmp(dato,"fail") == 0) {
            printf("[ERROR] Recibimos un FAIL");
            //return false;
            ok = 0;
        }
        else if (strcmp(dato,"") == 0) {
            printf("[ERROR] Recibimos una linea vacia");
            //return false;
            ok = 0;
        }
        else {
            char dato2[dato_size];
            strcpy(dato2,dato);
            char* token = strtok(dato, "\n");
            if (strstr(token, "FILE") != NULL){
                token = strtok(NULL,"\n");
                strcpy(*md5, token);
                int largo = 0;
                while (strtok(NULL,"\n") != NULL) {
                    largo++;
                }

                char* array_tokens [largo];
                int i = 0;
                token = strtok(dato2, "\n");
                token = strtok(NULL,"\n");
                while (i < largo) {
                    token = strtok(NULL,"\n");
                    array_tokens[i] = token;
                    i++;
                }
                int num = rand() % largo;

                token = strtok(array_tokens[num],":");

                strcpy(*ip,token);
                token = strtok(NULL,"\n");
                strcpy(*port,token);
                //return true;
            }
            else {
                printf("[ERROR] Esto fue lo que recibimos: %s",dato);
                //return false;
                ok = 0;
            }
        }
        printf("\n\n");
    }
    
    delete [] datoV;
    return ok;
}

int newClient(int listen_port, char* ip, int socket_tracker,int size_msg) {
        
        //Inicialización
        char * msg_to_terminal = new char[size_msg];
        bzero(msg_to_terminal,size_msg);
        char * msg_to_tracker  = new char[size_msg];
        bzero(msg_to_tracker,size_msg);
    
        //Dar de alta al cliente
        strcat(msg_to_terminal,"\n\n[OK] Cliente dado de alta ! \n\n");
        strcat(msg_to_tracker,"NEWCLIENT\n");
 
        char* aux = new char[20];
        bzero(aux,20);
        bool ok = 0;
        sprintf(aux,"%s:%i\n",ip,listen_port);
        strcat(msg_to_tracker,aux);

        sendMsgToSocket(msg_to_tracker, socket_tracker, 1);
        char* ip_aux = new char[16];
        bzero(ip_aux,16);
        char* port_aux = new char[6];
        bzero(port_aux,6);
        char* md5_aux = new char[32];
        bzero(md5_aux,32);
        ok = recvMsgToSocket(socket_tracker, &ip_aux, &port_aux, &md5_aux);
        delete [] aux;
        delete [] ip_aux;
        delete [] port_aux;
        delete [] md5_aux;
        
        delete [] msg_to_terminal;
        delete [] msg_to_tracker;
     
     return ok;
}

int parseCommand(char* cmd,char* prm,char* buffer) {
        int tam_prm = 256;
        int i   = 0;
        char c  = buffer[i];
	cmd[i]= buffer[i];
        while (c!=' ' && i<8){
		i++;
		cmd[i]= buffer[i];
		c=buffer[i];	
        }
	
        if (c==' ')
                cmd[i]='\0';
        if (strcmp(cmd,"share") != 0 && strcmp(cmd,"download") != 0 && strcmp(cmd,"show") != 0 && strcmp(cmd,"quit") != 0)
            return 0;
	i++;
	c=buffer[i];
        if (strcmp(cmd,"quit") != 0) {
            if (c==' ' || c=='\0')
                return 0;
        }
	int j=0;
	while (c!='\0' && c!='\n' && c!=' ' && j<tam_prm){
		prm[j]= buffer[i];
		i++;
		j++;
		c=buffer[i];	
        }
	prm[j]='\0';
        return 1;
}

char* getFileName(char* filename,char* buffer,const char* shared_folder_path) {
        
        char* res = new char[MAX_MSG_SIZE];
        bzero(res,MAX_MSG_SIZE);
        
        int i           = 0;
        int j           = 0;
        int k           = 0;
        int largo = strlen(shared_folder_path);
        while (largo > 0) {
//            printf("Largo: %i\n",largo);
            filename[j] = shared_folder_path[j];
            j++;
            largo--;
        }
        char c          = buffer[i];
        while (c!=':') { // Consumo la palabra "FILE:"
            i++;
            c       = buffer[i];	
        }
        while (c!='\n'){
            i++;
            filename[j]  = buffer[i];
            j++;
            res[k] = buffer[i];
            k++;
            c       = buffer[i];	
        }
	filename[j-1]='\0'; // @FIXME ver este -1
//	filename[j]='\0'; // @FIXME ver este -1
        res[k-1]='\0'; // @FIXME ver este -1
//        res[k]='\0'; // @FIXME ver este -1
        return res;
//        printf("Full_filename: %s---\n",filename);
//        printf("filename %s\n",filename);
}

// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *********************************** SE USA ? ********************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
void reemplazarEspacios(char* str_copy,char* str) {
    int largo = strlen(str);
    int i = 0;
    char c = str[i];	
    int j=0;
    while (largo > 0) {
        if (c==' ') {
            str_copy[j] = '\\';
            j++;
        }
        str_copy[j] = c;
        i++;
        c = str[i];
        largo--;
        j++;
    }
}

int clientAdministration(int terminal_client_socket, int client_tracker_socket, char* buffer,struct pollfd* fds, int   &nfds, archivos* s_archivos,StrMap*  map_archivos, StrMap*  map_clientes, const char* shared_folder_path, int listen_port) {
        /*char* data;
        data = buffer;*/
//        printf("\tDEBUG :: %s :: %u \n", buffer,(unsigned)strlen(buffer));
//        printf("\tDEBUG :: %s :: %u \n", data,(unsigned)strlen(data));
    
        printf("-------------------------------------------\n\n");
    
        char * cmd = new char[9]; 
        char * prm = new char[256]; 
        int res = parseCommand(cmd,prm,buffer);
        char* msg_to_terminal       = new char[MAX_MSG_SIZE];
        bzero(msg_to_terminal,MAX_MSG_SIZE);
//        printf("\tDEBUG :: %s :: %s :: %s :: \n", buffer, cmd, prm);
        int exit = 0;
        
        if (res == 0) { // NO ES UN COMANDO VALIDO
            printf("%s\n",buffer);
            printf("[ERROR] Comando invalido\n");
            strcat(msg_to_terminal,"[ERROR] Comando invalido\n");
            strcat(msg_to_terminal,"\n-------------------------------------------");

            strcat(msg_to_terminal,"\n\n");
            sendMsgToSocket(msg_to_terminal, terminal_client_socket, 2);               // Envío al terminal un mensaje
        }
        else {  // ES UN COMANDO VALIDO
        
            bool ok;

            char* msg_to_tracker        = new char[MAX_MSG_SIZE];
            bzero(msg_to_tracker,MAX_MSG_SIZE);

            strcat(msg_to_terminal,"\n");

            printf("%s %s\n",cmd,prm);
             // Analizo que comando se solicito
             if (strcmp(cmd,"quit") == 0) {
                strcat(msg_to_terminal,"[OK] Te fuiste !");
                exit = 1; // Voy a cerrar todo el cliente
             }
             else if (strcmp(cmd,"share") == 0) {
                    if (prm != '\0' && strcmp(prm,"") > 0) {
                        char* md5 = new char[MAX_MSG_SIZE];
                        bzero(md5,MAX_MSG_SIZE);
                        char* md5_aux = calculoMD5(prm,md5,shared_folder_path);
                        if (strstr(md5_aux,"[ERROR] No existe el archivo\n") == NULL) {
                                strcat(msg_to_tracker,"PUBLISH\n");
                                strcat(msg_to_tracker,prm);
                                strcat(msg_to_tracker,"\n");        
                                strcat(msg_to_tracker,md5);

                                sendMsgToSocket(msg_to_tracker, client_tracker_socket, 1);
                                char* ip_aux   = NULL;
                                char* port_aux = NULL;
                                char* md5_aux2 = NULL;
                                ok = recvMsgToSocket(client_tracker_socket,&ip_aux,&port_aux, &md5_aux2);
                                addFileOnClient(map_archivos, prm, md5_aux, 0, CLIENT_IP, listen_port, SHARED); //**********AL FINAL NO SE SI EL TAMAÑO SE USA PARA ALGO *****************************
                                //free(md5_aux);
                                if (!ok) {
                                    strcat(msg_to_terminal,"[ERROR] No se pudo compartir el archivo\n");
                                    strcat(msg_to_terminal,"Es posible que el tracker este cerrado.\n");
                                    strcat(msg_to_terminal,"Cerrando el cliente.\n");
//                                    printf("[ERROR] No se pudo compartir el archivo\n");
//                                    printf("Es posible que el tracker este cerrado\n");
                                    exit = 1;
                                }
                                else {
                                    strcat(msg_to_terminal,"[OK] Compartiendo el archivo: ");
                                    strcat(msg_to_terminal,prm);
                                }
    //                            printf("--LLEGO--\n");
                        }
                        else {
//                                printf("[ERROR] No existe el archivo\n");
                                strcat(msg_to_terminal,md5_aux);
                        }
                        delete [] md5;
                    }
                    else {
                        printf("[ERROR] Comando invalido\n");
                        strcat(msg_to_terminal,"[ERROR] Comando invalido\n");
                    }
             }
             else if (strcmp(cmd,"download") == 0) {
                 if (prm != '\0' && strcmp(prm,"") > 0) {
                    Entries     *firstD, *nextD;
                    fileEntry   *targetD;
                    bool        alreadyD = false;
                    showClientDownloads(map_archivos, firstD); 
                    targetD = firstD->current;                            
                    while(targetD != NULL) {
                        nextD      = firstD->next;
                        if (strncmp(prm, targetD->getName(), strlen(prm)) == 0) {
                            alreadyD = true;
                        }
                        delete firstD;
                        if(nextD == NULL) {
                            targetD = NULL;
                        }
                        else {
                                targetD = nextD->current;
                        }
                        firstD = nextD;
                    } 
                    if(!alreadyD) {       
                            
                            
                        strcat(msg_to_tracker,"SEARCH\n");
                        strcat(msg_to_tracker,prm);

                        sendMsgToSocket(msg_to_tracker, client_tracker_socket, 1);

                        char* ip = new char[16];
                        bzero(ip,16);
                        char* port = new char[6];
                        bzero(port,6);
                        char* md5 = new char[33];
                        bzero(md5,33);
                        ok = recvMsgToSocket(client_tracker_socket,&ip,&port,&md5);
                        if (!ok) {
                            strcat(msg_to_terminal,"[ERROR] El archivo no existe\n");
//                            strcat(msg_to_terminal,"Es posible que el tracker este cerrado.\n");
//                            strcat(msg_to_terminal,"Cerrando el cliente.\n");
//                            printf("[ERROR] El archivo no existe\n");
//                            printf("Es posible que el tracker este cerrado\n");
//                            exit = 1;
                        }
                        else {
                            
                            if (!((strcmp(ip,CLIENT_IP) == 0)  && atoi(port) == listen_port )) { // soy yo mismo
                                strcat(msg_to_terminal,"[OK] Descargando el archivo: ");
                                strcat(msg_to_terminal,prm);
                                /***************************/
                                /* Me conecto a un cliente */
                                /***************************/


                                int download_socket = socket(AF_INET, SOCK_STREAM, 0); 
                                struct addrinfo hintss, *add;
                                memset(&hintss, 0, sizeof hintss);
                                hintss.ai_family = AF_INET;
                                hintss.ai_socktype = SOCK_STREAM;

                                getaddrinfo(ip, port, &hintss, &add);
                                int error = connect(download_socket, add->ai_addr, add->ai_addrlen);
                                freeaddrinfo(add);

                                if (error < 0) { 
                                    perror("[ERROR] connect()"); 
                                    close(download_socket); 
                                    strcat(msg_to_terminal,"[ERROR] No se pudo crear la conexion");
                                    return 2; 
                                }
                                else {
                                    printf("[OK] Conexion con el uploader creada correctamente\n");
                                    // ACA SE ENVIA EL NOMBRE DEL ARCHIVO QUE QUIERO (¿EL MD5? TAMBIEN) --- DEL OTRO LADO RECIBE 
                                    char* msg_to_client       = new char[MAX_MSG_SIZE];
                                    bzero(msg_to_client,MAX_MSG_SIZE);
                                    strcat(msg_to_client,"FILE:");
                                    strcat(msg_to_client,prm);
                                    strcat(msg_to_client,"\n");
    //                                printf("Mensaje al uploader %s", msg_to_client);
                                    /*int bytes_enviados = */send(download_socket,msg_to_client,MAX_MSG_SIZE,0);
                                    // @FIXME manejo de errores del send anterior
                                    delete [] msg_to_client;

                                    fds[nfds].fd        = download_socket;
                                    fds[nfds].events    = POLLIN | POLLRDHUP; // Eventos que escucha
                                    char* path_file_dest       = new char[MAX_MSG_SIZE];
                                    bzero(path_file_dest,MAX_MSG_SIZE);
                                    strcat(path_file_dest,shared_folder_path);
            //                        strcat(path_file_dest,"cliente2/"); // @FIXME CAMBIAR path_file!!
                                    strcat(path_file_dest,prm);
            //                        printf("--%s--\n",path_file_dest);
                                    FILE *archivo_bajada = fopen(path_file_dest,"wb");
                                    if(archivo_bajada == NULL) {
                                        printf("[ERROR] El fichero no puede ser creado.\n");
                                        //return 1;
                                        // @FIXME CAMBIAR EL ORDEN PORQUE SI NO SE PUEDE CREAR EL ARCHIVO TENEEMOS QUE CERRAR EL CLOSET
                                    }
                                    FILE *archivo_bajada_md5 = fopen(path_file_dest,"rb");
                                    if(archivo_bajada_md5 == NULL) {
                                        printf("[ERROR] El fichero no puede ser creado.\n");
                                        //return 1;
                                        // @FIXME CAMBIAR EL ORDEN PORQUE SI NO SE PUEDE CREAR EL ARCHIVO TENEEMOS QUE CERRAR EL CLOSET
                                    }
                                    s_archivos[download_socket].archivo = archivo_bajada;
                                    s_archivos[download_socket].archivo_md5 = archivo_bajada_md5;

                                    delete [] path_file_dest;

                                    nfds++;
                                    char *socket_char = new char[3];
                                    sprintf(socket_char, "%d", download_socket);
                                    int portInt = atoi(port);
            //                        printf("Llego %i\n",portInt);
                                    addFileOnTracker(map_clientes, socket_char, prm, ip, portInt);
                                    delete [] socket_char;
            //                        printf("2Llego %i\n",portInt);

            //                        printf("IP: %s\n",ip);
            //                        printf("PORT: %i\n",portInt);
            //                        printf("FILENAME: %s\n",md5);

                                    addFileOnClient(map_archivos, prm, md5, 0, ip, portInt, DOWNLOADING);
            //                        printf("3Llego %i::%s::%s::\n",portInt,prm,md5);
            //                        printf("1-------------------\n");
            //                        printAll(map_archivos);
            //                        printf("2-------------------\n");
            //                        printAll(map_clientes);
            //                        printf("3-------------------\n");


                                    // @FIXME delete [] path_file_dest;
                                }
                                
                            }
                            else {
                                    // ya tenes este archivo
                                    strcat(msg_to_terminal,"[ERROR] Ya posee este archivo en su carpeta\n");
                            }
                            /**************************************************/
                            /* Fin de la definicion de la conexion al tracker */
                            /**************************************************/
                        }
                        delete [] ip;
                        delete [] port;
                        delete [] md5;
                    }
                 }
                 else {
//                        printf("%s",msg_to_terminal);
                        strcat(msg_to_terminal,"[ERROR] Comando invalido\n");
                 }
             }
             else if (strcmp(cmd,"show") == 0) {
                 Entries     *first, *next;
                 fileEntry   *target;
                 char        *ipReport, *nameReport;
                 unsigned int portReport;
                 State requestType = ANY;
                 if (strcmp(prm,"share") == 0) {
                            strcat(msg_to_terminal,"[OK] Estos son los archivos compartidos:\n");
                            showClientShares(map_archivos, CLIENT_IP, listen_port, first);
                            requestType = SHARED;
                 }
                 else if (strcmp(prm,"uploads") == 0) {
                            strcat(msg_to_terminal,"[OK] Estos son los envios en progreso:\n");
                            showClientUploads(map_archivos, first);
                            requestType = UPLOADING;
                 }
                 else if (strcmp(prm,"downloads") == 0) {
                            strcat(msg_to_terminal,"[OK] Estas son las descargas en progreso:\n");
                            showClientDownloads(map_archivos, first);
                            requestType = DOWNLOADING;
                 }
                 else {
                    strcat(msg_to_terminal,"[ERROR] Comando invalido\n"); 
                 }
//                 printf("%s",msg_to_terminal);
                 if(requestType != ANY) {
                    target = first->current;
                    char *tmpLine = new char[160]; //hasta 6 archivos se soporta bien calculo
                    bzero(tmpLine, 160);
                    while(target != NULL) {
                        next       = first->next;
                        ipReport   = target->getIP();
                        portReport = target->getPort();
                        nameReport = target->getName();
                        if(requestType == SHARED) {
                            sprintf(tmpLine, "%s (%lu bytes)\n",
                                    nameReport,
                                    target->getBytesUploaded()
                            );                                               
                        }
                        else if(requestType == UPLOADING) {
                            sprintf(tmpLine, "%s (%lu bytes) --> <%s:%i>\n", 
                                    nameReport, 
                                    target->getBytesUploaded(),
                                    ipReport,
                                    portReport
                            );
                        }
                        else if(requestType == DOWNLOADING) {
                            sprintf(tmpLine, "%s (%lu bytes) <-- <%s:%i>\n", 
                                    nameReport, 
                                    target->getBytesDownloaded(),
                                    ipReport,
                                    portReport
                            );
                        }
    //                    free(first);
                        delete first;
                        if(next == NULL) {
                            target = NULL;
                        }
                        else {
                                target = next->current;
                        }
                        first = next;
                        strcat(msg_to_terminal, tmpLine);
                    }
                    delete [] tmpLine;
                 }
             }
             else {
//                 printf("[ERROR] Comando invalido\n");
                 strcat(msg_to_terminal,"[ERROR] Comando invalido\n");
             }
//             printf("\n\n");

            strcat(msg_to_terminal,"\n-------------------------------------------");

            strcat(msg_to_terminal,"\n\n");
            printf("%s",msg_to_terminal);
            sendMsgToSocket(msg_to_terminal, terminal_client_socket, 2);               // Envío al terminal un mensaje

            delete [] msg_to_tracker;
            //delete [] md5;
        }
        
        delete [] msg_to_terminal;       
        delete [] cmd;
        delete [] prm;
        
        return exit;

}
