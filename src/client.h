/* 
 * File:   cliente.h
 * Author: grupo03
 *
 * Created on September 19, 2013, 10:06 PM
 */

#ifndef CLIENTE_H
#define	CLIENTE_H

struct archivos {
    FILE* archivo;
    FILE* archivo_md5;
};

void print_pollfd(struct pollfd *pfd);

char* calculoMD5(const char* filename, char* res, const char* shared_folder_path);

char* calculoMD5_2(FILE* inFile,char* res);

void sendMsgToSocket (char* msg, int socket, int imp);

bool recvMsgToSocket (int socket,char** ip, char** port);

int newClient(int listen_port, char* ip, int socket_tracker,int size_msg);

int clientAdministration(int terminal_client_socket, int client_tracker_socket,char* buffer, struct pollfd* pfd, int &nfds, archivos* s_archivos,StrMap*  map_archivos, StrMap*  map_clientes, const char* shared_folder_path, int listen_port);

char* getFileName(char* filename,char* buffer,const char* shared_folder_path);

int parseCommand(char* cmd,char* prm,char* buffer);

#endif	/* CLIENTE_H */

