#include <string.h>
#include "fileEntry.h"

fileEntry::fileEntry(char* name, char* md5, char* ip, unsigned int port) {
    this->name            = name;
    this->md5             = md5;
    this->ip              = ip;
    this->port            = port;
    
    this->size            = 0;
    this->bytesDownloaded = 0;
    this->bytesUploaded   = 0;
    this->state           = SHARED;
}

fileEntry::fileEntry(char* name, char* md5, unsigned int size, char* ip, unsigned int port, State state) {
    this->name            = name;
    this->md5             = md5;
    this->ip              = ip;
    this->port            = port;
    
    this->size            = size;
    this->bytesDownloaded = 0;
    this->bytesUploaded   = 0;
    this->state           = state;
}

char* fileEntry::getName() const {
    return name;
}

char* fileEntry::getMD5() const {
    return md5;
}

void fileEntry::setMD5(char *md5) {
    this->md5 = md5;
}

unsigned int fileEntry::getSize() const {
    return size;
}

void fileEntry::setSize(unsigned int size) {
    this->size = size;
}

char* fileEntry::getIP() const {
    return ip;
}

unsigned int fileEntry::getPort() const {
    return port;
}

unsigned long fileEntry::getBytesDownloaded() const {
    return bytesDownloaded;
}

void fileEntry::setBytesDownloaded(unsigned long bytes) {
    this->bytesDownloaded = bytes;
}

unsigned long fileEntry::getBytesUploaded() const {
    return bytesUploaded;
}

void fileEntry::setBytesUploaded(unsigned long bytes) {
    this->bytesUploaded = bytes;
}

State fileEntry::getState() const {
    return state;
}

void fileEntry::setState(State currentState) {
    this->state = currentState;
}

fileEntry::~fileEntry() {
    if (this->name != NULL) {
        delete [] this->name;
    }
    if (this->md5 != NULL) {
        delete [] this->md5;
    }
    if (this->ip != NULL) {
        delete [] this->ip;
    }
}
