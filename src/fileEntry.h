/* 
 * File:   fileEntry.h
 * Author: grupo03
 *
 * Created on August 31, 2013, 12:10 PM
 */

#ifndef FILEENTRY_H
#define	FILEENTRY_H
enum State { ANY, SHARED, DOWNLOADING, UPLOADING };

class fileEntry {
public:    
    
    fileEntry(char* name, char* md5, char* ip, unsigned int port);
    fileEntry(char* name, char* md5, unsigned int size, char* ip, unsigned int port, State state);
    virtual ~fileEntry();
    
    char*         getName() const;
    char*         getMD5()  const;
    void          setMD5(char *md5);
    unsigned int  getSize() const;
    void          setSize(unsigned int size);
    char*         getIP()   const;
    unsigned int  getPort() const;        
    unsigned long getBytesDownloaded() const;
    void          setBytesDownloaded(unsigned long bytes);
    unsigned long getBytesUploaded() const;
    void          setBytesUploaded(unsigned long bytes);
    virtual State getState() const;
    void          setState(State currentState);

private:
    char*         name;
    char*         md5;
    unsigned int  size;
    char*         ip;
    unsigned int  port;
    unsigned long bytesDownloaded;   
    unsigned long bytesUploaded;
    State         state;
};
#endif /* FILEENTRY_H */
