#ifndef _STRMAP_H_
#define _STRMAP_H_

#include <stdlib.h>
#include <string.h>
#include "fileEntry.h"    

typedef struct Entries Entries;
typedef struct StrMap  StrMap;
typedef struct Bucket  Bucket;
typedef struct Pair    Pair;

struct Entries {
    fileEntry *current;
    Entries   *next;
};

typedef void(*sm_enum_func)(const char *ip, unsigned int port, Bucket *bucket, Pair *currentPair, fileEntry* &fileEntry);

StrMap * newHash(unsigned int capacity);

void deleteHash(StrMap *map);

int search(const StrMap *map, std::string fileName, Entries* &entries);

int getFile(const StrMap *map, const char *fileName, const char *ip, unsigned int port, fileEntry* &target);

int addFileOnClient(StrMap *map, std::string name, std::string md5, unsigned int size, std::string ip, unsigned int port, State downloadState);

int addFileOnTracker(StrMap *map, std::string name, std::string md5, std::string ip, unsigned int port);

void deleteClient(StrMap *map, const char *ip, unsigned int port);

void showClientShares(StrMap *map, const char *ip, unsigned int port, Entries* &entries);

void showClientDownloads(StrMap *map, Entries* &entries);

void showClientUploads(StrMap *map, Entries* &entries);

int finishUpload(StrMap *map, const char *fileName, const char *ip, unsigned int port);

int finishDownload(StrMap *map, const char *fileName, const char *ip, unsigned int port);

/*DEVELOPMENT TOOLS*/

int deleteFile(StrMap *map, const char *fileName, const char *ip, unsigned int port, State state);

bool exists(const StrMap *map, const char *fileName);

int getTotalCount(const StrMap *map);

void printAll(StrMap *map);

void printClient(StrMap *map, const char *ip, unsigned int port);

#endif

